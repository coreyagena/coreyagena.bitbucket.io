var classcontroller_1_1Controller =
[
    [ "__init__", "classcontroller_1_1Controller.html#a26be920ef56d5227b57f65bea6c6e242", null ],
    [ "get_Kp", "classcontroller_1_1Controller.html#aa6f1a97f3a1f0da304c761d4f61a82f5", null ],
    [ "set_Kp", "classcontroller_1_1Controller.html#aa24acbd7e12ceea421b6ac52f800b617", null ],
    [ "update", "classcontroller_1_1Controller.html#a07d7fa15ecb7e8ba37a9e4e9fa3f0d49", null ],
    [ "duty", "classcontroller_1_1Controller.html#ae063e03f2e23e29c9a553544031a1498", null ],
    [ "Kp", "classcontroller_1_1Controller.html#a34cf377d78ee193d7160d74adbe9aabf", null ],
    [ "L", "classcontroller_1_1Controller.html#ad8153fc6d4cabcaa48a91b1c3cc61b6c", null ],
    [ "omega_meas", "classcontroller_1_1Controller.html#a66c93884e10178cd85178770d44b5240", null ],
    [ "omega_ref", "classcontroller_1_1Controller.html#a675d552716b1de7f5a3a3c6df1d2041c", null ],
    [ "pwm_sat_low", "classcontroller_1_1Controller.html#aacd024bb4e646a31a0ea3147f35de6c3", null ]
];