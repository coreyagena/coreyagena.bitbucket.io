var task__user_8py =
[
    [ "task_user.Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ],
    [ "S0_INIT", "task__user_8py.html#ad172b5aee322276b27b03b3e3dd99680", null ],
    [ "S1_WAIT_FOR_CHAR", "task__user_8py.html#ac39b036962e5dd5a09522dcadc25723b", null ],
    [ "S2_COLLECT", "task__user_8py.html#ab46587a177c02c818d0f538c141b6ee2", null ],
    [ "S3_PRINT", "task__user_8py.html#a7e8305a953bb44ec5babd46b78ec1c32", null ],
    [ "S4_MOTOR", "task__user_8py.html#ae84edf47973c90d68ef405e9a0be3c49", null ],
    [ "S5_KP", "task__user_8py.html#a305ced2b275d33c290f2b5337ce5fd07", null ],
    [ "S6_VELOCITY", "task__user_8py.html#abf8304b29b888c5321f3a951eaa64620", null ],
    [ "S7_STEP", "task__user_8py.html#ac1e245c0e15f1dc296217caf7a58b604", null ],
    [ "S8_STEP_PRINT", "task__user_8py.html#a82fef805c96cc33c1aa114f904c273a6", null ]
];